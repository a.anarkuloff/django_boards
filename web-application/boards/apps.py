from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class WebappConfig(AppConfig):
    name = 'boards'
    verbose_name = _('Boards')
