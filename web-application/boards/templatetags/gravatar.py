import hashlib
from urllib.parse import urlencode

from django import template

register = template.Library()


@register.filter
def gravatar(user):
    email = user.email.lower().encode('utf-8')
    default = 'mm'
    size = 256
    url = 'https://www.gravatar.com/avatar/{md5}?{params}'.format(
        md5=hashlib.md5(email).hexdigest(),
        params=urlencode({'d': default, 's': str(size)})
    )
    return url

# aitpa.kg@mail.ru = 7da929753bf490afe11321fde0f8e234
# https://www.gravatar.com/avatar/7da929753bf490afe11321fde0f8e234?d=mm&s=256
